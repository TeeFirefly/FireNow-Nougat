package com.android.server.firefly;

import android.content.Context;
import android.util.Log;
import com.android.server.SystemService;

public final class FireflyManagerService extends SystemService {

    private static final String TAG = "FireflyManagerService";
    final FireflyManagerServiceImpl mImpl;

    public FireflyManagerService(Context context) {
        super(context);
        mImpl = new FireflyManagerServiceImpl(context);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "Registering service " + Context.FIREFLY_SERVICE);
        publishBinderService(Context.FIREFLY_SERVICE, mImpl);
    }

    @Override
    public void onBootPhase(int phase) {
        if (phase == SystemService.PHASE_SYSTEM_SERVICES_READY) {
            mImpl.start();
        }
    }
}